# Установка и запуск
## В корневой директории (с README.md) выполнить следующие комманды:

## Poetry
- ``poetry shell``  – запуск виртуальной среды
- ``poetry install`` – установка зависимостей из pyproject.toml (python 3.8 или поменять версию в этом файле)
- ``python main.py`` – запуск скрипта

## Pip
- ``pip install pyTelegramBotAPI`` – устанока пакета
- ``python main.py`` – запуск скрипта
